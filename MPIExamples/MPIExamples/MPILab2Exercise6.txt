﻿#include "stdafx.h";
#include <iostream>
#include <mpi.h>
#include <ctime>
#include <vector>
#include <stdio.h>
#include <string>
/*
6. Napisati MPI program kojim se kreira dvodimenzionalna Cartesian struktura sa n vrsta i m
kolona. Za svaki skup procesa koji pripadaju istoj koloni strukture kreirati novi komunikator.
Master procesu iz svake kolone poslati koordinate procesa sa najvećim identifikatorom i
prikazati ih.
*/
using namespace std;
#define N 3
#define M 4
#define V 7

void main(int argc, char* argv[])
{
	int myRank, size, cartesianRank;
	MPI_Comm cartesian_comm, cartesian_column_comm;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

	if (size != N * M)
	{
		if (myRank == 0)
			cout << "Invalid number of process\n";
		MPI_Finalize();
		return;
	}

	int ndims = 2;
	int* dim_size = new int[ndims];
	int* periods = new int[ndims];

	dim_size[0] = M;
	dim_size[1] = N;

	periods[0] = 1;
	periods[1] = 1;

	int reorder = 0;

	MPI_Cart_create(MPI_COMM_WORLD, ndims, dim_size, periods, reorder, &cartesian_comm);

	MPI_Comm_rank(cartesian_comm, &cartesianRank);

	int* coords = new int[ndims];
	MPI_Cart_coords(cartesian_comm, cartesianRank, ndims, coords);

	int color = coords[0];
	int key = cartesianRank;

	MPI_Comm_split(cartesian_comm, color, key, &cartesian_column_comm);

	int maxRank;
	int columnRank;
	MPI_Comm_rank(cartesian_column_comm, &columnRank);

	MPI_Reduce(&cartesianRank, &maxRank, 1, MPI_INT, MPI_MAX, 1, cartesian_column_comm);

	if (columnRank == 1)
	{
		MPI_Cart_coords(cartesian_comm, maxRank, ndims, coords);
		MPI_Send(coords, 2, MPI_INT, 0, 0, cartesian_column_comm);
	}
		

	if (columnRank == 0)
	{
		MPI_Status stat;
		int* receivedCoords = new int[2];
		MPI_Recv(receivedCoords, 2, MPI_INT, 1, 0, cartesian_column_comm, &stat);

		printf("myRank: %d, cartesianRank: %d, coords[%d,%d]\n", myRank, cartesianRank, receivedCoords[0], receivedCoords[1]);
	}

	MPI_Finalize();
}
/*
Output:
mpiexec -n 12 MPIExamples.exe
myRank: 0, cartesianRank: 0, coords[0,2]
myRank: 6, cartesianRank: 6, coords[2,2]
myRank: 3, cartesianRank: 3, coords[1,2]
myRank: 9, cartesianRank: 9, coords[3,2]
*/
