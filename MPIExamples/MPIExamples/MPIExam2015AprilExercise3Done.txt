﻿#include "stdafx.h";
#include <iostream>
#include <mpi.h>
#include <ctime>
#include <vector>
#include <stdio.h>
#include <string>
#include <math.h>
using namespace std;
#define M 3
#define K 12
/*
April 2015
Napisati MPI program koji realizuje množenje matrice Amxk i vektora Bk  čime se dobija rezultujući vektor c. 
Množenje se obavlja tako što master proces (sa rankom 0) šalje svakom procesu radniku po k/p (k je deljivo sa p) kolona 
matrice A i po k/p elemenata vektora b. Master proces ne učestvuje u izračunavanju. 
Predvideti da se slanje k/p kolona matrice A i k/p elemenata vektora b  svakom procesu obavlja 
sa po jednom naredbom MPI_Send.
*/

std::string getStringMatrixA(int mat[M][K], int n, int m, string desc, int rank)
{
	std::string str;
	str.append(desc);
	str.append(std::to_string(rank));
	for (int i = 0; i < n; i++)
	{
		str.append("\n");
		for (int j = 0; j < m; j++)
		{
			str.append(std::to_string((int)mat[i][j]));
			str.append(" ");
		}
	}
	return str;
}

std::string getStringVectorB(int vec[K], int n, string desc, int rank)
{
	std::string str;
	str.append(desc);
	str.append(std::to_string(rank));
	str.append("\n");
	for (int j = 0; j < n; j++)
	{
		str.append(std::to_string((int)vec[j]));
		str.append(" ");
	}
	return str;
}

std::string getStringVectorC(int vec[M], int n, string desc, int rank)
{
	std::string str;
	str.append(desc);
	str.append(std::to_string(rank));
	str.append("\n");
	for (int j = 0; j < n; j++)
	{
		str.append(std::to_string((int)vec[j]));
		str.append(" ");
	}
	return str;
}

void main(int argc, char* argv[])
{
	int myRank, size;
	int A[M][K];
	int B[K];
	int C[M];

	MPI_Datatype A_column_mpi_t;
	MPI_Datatype A_sub_column_mpi_t;

	MPI_Datatype B_vector_mpi_t;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (K % (size - 1) != 0)
	{
		if (myRank == 0)
			cout << "Invalid number of proceses.";
		MPI_Finalize();
		return;
	}

	int workerSize = size - 1;
	MPI_Type_vector(M, K / workerSize, K, MPI_INT, &A_column_mpi_t);
	MPI_Type_commit(&A_column_mpi_t);
	MPI_Type_create_resized(A_column_mpi_t, 0, K / workerSize * sizeof(int), &A_sub_column_mpi_t);
	MPI_Type_commit(&A_sub_column_mpi_t);

	MPI_Type_contiguous(K / workerSize, MPI_INT, &B_vector_mpi_t);
	MPI_Type_commit(&B_vector_mpi_t);

	if (myRank == 0)
	{
		cout << "Initial matrix A:";
		for (int i = 0; i < M; i++)
		{
			cout << endl;
			for (int j = 0; j < K; j++)
			{
				A[i][j] = (i + j) % 10;
				cout << A[i][j] << " ";
			}
		}
		cout << endl;
		cout << "Initial vector B:\n";
		for (int i = 0; i < K; i++)
		{
			B[i] = i;
			cout << B[i] << " ";
		}

		cout << endl;
		cout << "Initial result matrix:\n";
		for (int i = 0; i < M; i++)
		{
			C[i] = 0;
			cout << C[i] << " ";
		}

		fflush(stdout);
	}
	else
	{
		for (int i = 0; i < M; i++)
			for (int j = 0; j < K; j++)
				A[i][j] = 0;

		for (int i = 0; i < K; i++)
			B[i] = 0;

		for (int i = 0; i < M; i++)
			C[i] = 0;
	}

	if (myRank == 0)
	{
		int sendFrom = 0;
		for (int sendTo = 1; sendTo < size; sendTo++)
		{
			//send part of matrix
			MPI_Send(&(A[0][sendFrom]), 1, A_sub_column_mpi_t, sendTo, 0, MPI_COMM_WORLD);

			//send part of vector
			MPI_Send(&(B[sendFrom]), 1, B_vector_mpi_t, sendTo, 0, MPI_COMM_WORLD);

			sendFrom += (K / workerSize);
		}
	}
	else
	{
		int recvFrom = (myRank - 1)*K / workerSize;
		//receive part of matrix
		MPI_Recv(&(A[0][recvFrom]), 1, A_column_mpi_t, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);

		cout << getStringMatrixA(A, M, K, "\nMatrix A recv rank ", myRank);
		fflush(stdout);

		//receive part of vector
		MPI_Recv(&(B[recvFrom]), 1, B_vector_mpi_t, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);

		cout << getStringVectorB(B, K, "\nVector B recv rank ", myRank);
		fflush(stdout);

		for (int i = 0; i < M; i++)
		{
			C[i] = 0;
			for (int j = recvFrom; j < recvFrom + K / workerSize; j++)
				C[i] += A[i][j] * B[j];
		}

		cout << getStringVectorC(C, M, "\nResult matrix C rank ", myRank);
		fflush(stdout);
	}

	int result[M];
	MPI_Reduce(&(C[0]), &(result[0]), M, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	MPI_Barrier(MPI_COMM_WORLD);
	if (myRank == 0)
	{
		cout << getStringVectorC(result, M, "\nFinal result matrix C rank ", myRank);
		fflush(stdout);
	}

	MPI_Finalize();
}

