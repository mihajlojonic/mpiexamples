#include "stdafx.h";
#include <iostream>
#include <mpi.h>
#include <ctime>
#include <vector>
#include <stdio.h>
using namespace std;
#define SIZE 8

void fillMatrix(int m[SIZE][SIZE])
{
	int n = 0;
	for (int i = 0; i < SIZE; i++)
		for (int j = 0; j < SIZE; j++)
			m[i][j] = i + j;
}

void printMatrix(int m[SIZE][SIZE])
{
	for (int i = 0; i < SIZE; i++)
	{
		cout << endl;
		for (int j = 0; j < SIZE; j++)
			cout << m[i][j] << " ";
	}
}

int* getArray(int m[SIZE][SIZE])
{
	int lengthOfArray = SIZE * SIZE;
	int* array = new int[lengthOfArray];

	int k = 0;
	for (int i = 0; i < SIZE; i++)
		for (int j = 0; j < SIZE; j++)
			array[k++] = m[i][j];

	return array;
}

void main(int argc, char* argv[])
{
	int myRank, sizeWorld;
	int A[SIZE][SIZE], B[SIZE][SIZE], C[SIZE][SIZE];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &sizeWorld);

	if (SIZE % sizeWorld != 0)
	{
		if (myRank == 0) cout << "Matrix size not divisible by number of processors\n" << endl;
		MPI_Finalize();
		return;
	}

	int from = myRank * SIZE / sizeWorld;
	int to = (myRank + 1) * SIZE / sizeWorld;

	if (myRank == 0)
	{
		fillMatrix(A);
		fillMatrix(B);
	}
	
	MPI_Bcast(B, SIZE*SIZE, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Scatter(getArray(A), SIZE * SIZE / sizeWorld, MPI_INT, A[from], SIZE * SIZE / sizeWorld, MPI_INT, 0, MPI_COMM_WORLD);
	
	cout << "Computing slice" << myRank << " (from row " << from << " to "<< to-1 << ")" << endl;
	for(int i = from; i < to; i++)
		for (int j = 0; j < SIZE; j++)
		{
			C[i][j] = 0;
			for (int k = 0; k < SIZE; k++)
				C[i][j] += A[i][k] * B[k][j];
		}

	int* cArray = new int[SIZE * SIZE];
	MPI_Gather(C[from], SIZE * SIZE / sizeWorld, MPI_INT, cArray/*C*/, SIZE * SIZE / sizeWorld, MPI_INT, 0, MPI_COMM_WORLD);

	MPI_Barrier(MPI_COMM_WORLD);
	if (myRank == 0)
	{
		cout << endl << endl;
		printMatrix(A);
		cout << endl << endl;
		printMatrix(B);
		cout << endl << endl;
		
		int k = 0;
		for (int i = 0; i < SIZE; i++)
			for (int j = 0; j < SIZE; j++)
				C[i][j] = cArray[k++];

		printMatrix(C);
	}

	MPI_Finalize();
}

