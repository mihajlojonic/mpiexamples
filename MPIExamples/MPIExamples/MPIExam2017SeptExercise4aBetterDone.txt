#include "stdafx.h";
#include <iostream>
#include <mpi.h>
#include <ctime>
#include <vector>
#include <stdio.h>
#include <string>
using namespace std;
#define N 6

std::string getStringMatrix(int mat[N][N], int n, int m, string desc, int rank)
{
	std::string str;
	str.append(desc);
	str.append(std::to_string(rank));
	for (int i = 0; i < n; i++)
	{
		str.append("\n");
		for (int j = 0; j < m; j++)
		{
			str.append(std::to_string((int)mat[i][j]));
			str.append(" ");
		}
	}
	return str;
}

std::string getStringArray(int* arr, int n, string desc, int rank)
{
	std::string str;
	str.append(desc);
	str.append(std::to_string(rank));
	str.append(" >>> ");
	for (int j = 0; j < n; j++)
	{
		str.append(std::to_string((int)arr[j]));
		str.append(" ");
	}
	return str;
}

void main(int argc, char* argv[])
{
	int myRank, size;
	int A[N][N];
	int C[N][N];
	MPI_Datatype block_mpi_t;
	MPI_Datatype sub_block_mpi_t;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (N % 2 != 0)
	{
		if (myRank == 0)
			cout << "Invalid matrix dimension\n";
		MPI_Finalize();
		return;
	}

	if (size != 4)
	{
		if (myRank == 0)
			cout << "Invalid number of prcess.Start 4 proceses.\n";
	}

	//init matrix C
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
			C[i][j] = 0;
	}

	//init matrix A
	if (myRank == 0)
	{
		cout << "Initial matrix A:";
		for (int i = 0; i < N; i++)
		{
			cout << endl;
			for (int j = 0; j < N; j++)
			{
				A[i][j] = (rand() % 10)  + 1;
				if (A[i][j] == 10) A[i][j]--;
				cout << A[i][j] << " ";
			}
		}
		cout << endl;
	}

	int temp[3] = { 1, N, N+1 };

	if (myRank == 0)
	{
		//creating block lengths
		int numIntsSend = (N*(N + 1)) / 2 - 2;

		int* blockLen = new int[numIntsSend];
		for (int i = 0; i < numIntsSend; i++)
			blockLen[i] = 1;

		int* displ = new int[numIntsSend];

		for (int sendTo = 1; sendTo < size; sendTo++)
		{
			//creating displacements
			for (int i = 0; i < numIntsSend; i++)
			{
				if (i % ((N/2)*(N/2)) == 0) 
					displ[i] = temp[sendTo - 1] * N / 2;
				else
					if (i % (N / 2) == 0)
						displ[i] = displ[i - 1] + N / 2 + 1;
					else
						displ[i] = displ[i - 1] + 1;
			}
				
			MPI_Type_indexed(numIntsSend, blockLen, displ, MPI_INT, &block_mpi_t);
			MPI_Type_commit(&block_mpi_t);
			MPI_Send(&(A[0][0]), 1, block_mpi_t, sendTo, 0, MPI_COMM_WORLD);
		}
	}
	else
	{
		//creating block lengths
		int* blockLen = new int[N];
		for (int i = 0; i < N - 1; i++)
			blockLen[i] = i + 1;
		blockLen[N - 1] = N - 2;

		//creating displacements
		int* displ = new int[N];
		displ[0] = N - 1;
		for (int i = 1; i < N ; i++)
			displ[i] = displ[i - 1] + N - 1; 
		
		MPI_Type_indexed(N, blockLen, displ, MPI_INT, &block_mpi_t);
		MPI_Type_commit(&block_mpi_t);
		MPI_Recv(&(C[0][0]), 1, block_mpi_t, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);

		cout << getStringMatrix(C, N, N, "\nIn C after recv rank ", myRank);
		fflush(stdout);
	}

	MPI_Finalize();
}

