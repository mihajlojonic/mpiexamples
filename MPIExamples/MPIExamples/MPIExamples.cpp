#include "stdafx.h";
#include <iostream>
#include <mpi.h>
#include <ctime>
#include <vector>
#include <stdio.h>
#include <string>
/*
3. Napisati MPI program koji kreira komunikator comm1 koji se sastoji od svih procesa sa
identifikatorima deljivim sa 3. Master proces (P0) svim procesima ove grupe šalje po jednu
vrstu matrice A. Odštampati identifikatore procesa koji pripadaju comm1 i čija je suma
elemenata primljene vrste matrice A manja od zadate vrednosti v.
*/
using namespace std;
#define N 3
#define V 7

std::string getStringMatrix(int mat[N][N], int n, int m, string desc, int rank)
{
	std::string str;
	str.append(desc);
	str.append(std::to_string(rank));
	for (int i = 0; i < n; i++)
	{
		str.append("\n");
		for (int j = 0; j < m; j++)
		{
			str.append(std::to_string((int)mat[i][j]));
			str.append(" ");
		}
	}
	return str;
}

std::string getStringArray(int* arr, int n, string desc, int rank)
{
	std::string str;
	str.append(desc);
	str.append(std::to_string(rank));
	str.append(" >> ");
	for (int i = 0; i < n; i++)
	{
		str.append(std::to_string(arr[i]));
		str.append(" ");
	}
	return str;
}

void main(int argc, char* argv[])
{
	int myRank, size;
	int myRankComm1;
	int A[N][N];
	int receivedRow[N];

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

	if (size != N * N)
	{
		if (myRank == 0)
			cout << "Invalid number of process\n";
		MPI_Finalize();
		return;
	}

	MPI_Group group_world, group_divide_by_3;
	MPI_Comm world_comm, comm1;
	world_comm = MPI_COMM_WORLD;

	if (myRank == 0)
	{
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				A[i][j] = i + j + 1;

		cout << getStringMatrix(A, N, N, "Init matrix, rank: ", myRank);
	}

	int membersCount = size / 3;
	int* members = new int[membersCount];

	for (int i = 0; i < size; i = i + 3)
		members[i / 3] = i;

	if (myRank == 0)
	{
		cout << getStringArray(members, membersCount, "\nArray members:", myRank);
	}

	MPI_Comm_group(MPI_COMM_WORLD, &group_world);

	MPI_Group_incl(group_world, membersCount, members, &group_divide_by_3);
	MPI_Comm_create(world_comm, group_divide_by_3, &comm1);

	MPI_Datatype row_type;

	MPI_Type_vector(1, N, N, MPI_INT, &row_type);
	MPI_Type_commit(&row_type);

	MPI_Barrier(MPI_COMM_WORLD);
	if (myRank % 3 == 0)
	{
		MPI_Comm_rank(comm1, &myRankComm1);
		MPI_Scatter(&(A[0][0]), 1, row_type, &(receivedRow[0]), 1, row_type, 0, comm1);

		cout << getStringArray(receivedRow, N, "\nReceived row rank: ", myRank);
		fflush(stdout);

		int sum_row = 0;
		for (int i = 0; i < N; i++)
			sum_row += receivedRow[i];
		if (sum_row > V)
			printf("\nWorld rank: %d, Comm1 rank: %d, Sum of array is %d (MORE then %d)", myRank, myRankComm1, sum_row, V);
		else
			printf("\nWorld rank: %d, Comm1 rank: %d, Sum of array is %d (LESS then %d)", myRank, myRankComm1, sum_row, V);

		fflush(stdout);
	}


	MPI_Finalize();
}

/*
Output:
mpiexec -n 9 MPIExamples.exe
Init matrix, rank: 0
1 2 3
2 3 4
3 4 5
Array members:0 >> 0 3 6
Received row rank: 0 >> 1 2 3

Received row rank: 6 >> 3 4 5

Received row rank: 3 >> 2 3 4

World rank: 0, Comm1 rank: 0, Sum of array is 6 (LESS then 7)

World rank: 6, Comm1 rank: 2, Sum of array is 12 (MORE then 7)

World rank: 3, Comm1 rank: 1, Sum of array is 9 (MORE then 7)
*/

